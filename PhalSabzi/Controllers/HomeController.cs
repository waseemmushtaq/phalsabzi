﻿using Microsoft.AspNet.Identity;
using PhalSabzi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PhalSabzi.Controllers
{
    public class HomeController : Controller
    {
        PhalSabziEntities db = new PhalSabziEntities();
        public ActionResult Index(string item)
        {
            List<Item> list = new List<Item>();
            if (TempData["Message"] != null)
                ViewBag.Message = TempData["Message"].ToString();
            if (item != null && item != "")
            {
                if (item == "All Products")
                {
                    list = db.Items.ToList();
                    return PartialView("~/Views/Home/_ItemlistPartial.cshtml", list);
                }
                else
                {
                    list = db.Items.Where(x => x.Category == item).ToList();
                    return PartialView("~/Views/Home/_ItemlistPartial.cshtml", list);
                }
            }
            else
            {
                list = db.Items.ToList();
                return View(list);
            }
            //return View(list);
        }

        //[HttpPost]
        public JsonResult checkemail(string email)
        {
            var find = db.AspNetUsers.Where(x => x.Email == email).FirstOrDefault();
            if (find != null)
            {
                return Json(new { res = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { res = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult View(int? id, string Veg, string Fruit)
        {
            var userid = User.Identity.GetUserId();
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.address = db.AspNetUsers.Where(x => x.Id == userid).Select(x => x.Address).First();
            }
            if (TempData["Message"] != null)
                ViewBag.Message = TempData["Message"].ToString();

            ViewBag.catcount = db.Items
                    .GroupBy(p => p.Category)
                    .Select(g => new cat { id = g.Key, count = g.Count() });
            ProductViewModel mod = new ProductViewModel();
            mod.item = db.Items.Where(x => x.ID == id).FirstOrDefault();
            if (mod.item.Category == "Vegitables")
            {
                mod.items = db.Items.Where(x => x.Category == "Vegitables" && x.ID != id).Take(3).ToList();
            }
            else
            {
                mod.items = db.Items.Where(x => x.Category == "Fruits" && x.ID != id).Take(3).ToList();
            }
            mod.feedback = db.Feedbacks.Where(x => x.Item_ID == id).ToList();
            return View(mod);
        }

        [Authorize]
        public ActionResult AddCart(int? id, Cart mod)
        {
            var userid = User.Identity.GetUserId();
            var checkchat = db.Carts.Where(x => x.User_ID == userid && x.Item_ID == id && x.Enable_Flag == "New").FirstOrDefault();
            if (checkchat == null)
            {
                var data = db.Items.Where(x => x.ID == id).FirstOrDefault();
                if (data.Quantity != "0")
                {
                    if (data.Type == "Killo")
                    {
                        int quan = int.Parse(data.Quantity) - int.Parse(mod.Quantity);
                        data.Quantity = quan.ToString();
                    }
                    else
                    {
                        //var total =  int.Parse(data.Quantity) - int.Parse(mod.Quantity) ;
                        //data.Quantity = total.ToString();
                    }
                    if (data.Sales != null)
                    {
                        int sale = int.Parse(data.Sales);
                        double percent = (double)sale / 100;
                        double ans = Convert.ToInt32(percent * data.Price);
                        mod.Total = Convert.ToInt32(data.Price - ans);
                    }
                    else
                    {
                        mod.Total = data.Price;
                    }
                    mod.Creation_Date = DateTime.Now;
                    mod.User_ID = User.Identity.GetUserId();
                    mod.Item_ID = id;
                    mod.Enable_Flag = "New";
                    db.Carts.Add(mod);
                    db.SaveChanges();
                    TempData["Message"] = "Your Item is Added into Cart Successfully!";
                    var values = RouteDataContext.RouteValuesFromUri(Request.UrlReferrer);
                    var actionName = values["action"].ToString();
                    if (actionName == "Index")
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("View", new { id = mod.Item_ID });
                    }
                }
                else
                {
                    var values = RouteDataContext.RouteValuesFromUri(Request.UrlReferrer);
                    var actionName = values["action"].ToString();
                    if (actionName == "Index")
                    {
                        TempData["Message"] = "Sorry Item is out of Stock!";
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        TempData["Message"] = "Sorry Item is out of Stock!";
                        return RedirectToAction("View", new { id = id });
                    }
                }
            }
            else
            {
                var values = RouteDataContext.RouteValuesFromUri(Request.UrlReferrer);
                var actionName = values["action"].ToString();
                if (actionName == "Index")
                {
                    TempData["Message"] = "This item is already in your cart!";
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    TempData["Message"] = "This item is already in your cart!";
                    return RedirectToAction("View", new { id = id });
                }
            }
        }

        public ActionResult Feedback(Feedback mod, int? id, int rating)
        {
            mod.Item_ID = id;
            mod.Customer_ID = User.Identity.GetUserId();
            mod.Creation_Date = DateTime.Now;
            db.Feedbacks.Add(mod);
            var find = db.Items.Find(id);
            find.Rating = rating;
            db.SaveChanges();
            TempData["Message"] = "Your Feedback Added Successfully!";
            return RedirectToAction("View", new { id = mod.Item_ID });
        }

        public ActionResult Contact()
        {
            if (TempData["Message"] != null)
                ViewBag.Message = TempData["Message"].ToString();
            return View();
        }

        [HttpPost]
        public ActionResult Contact(ContactU mod)
        {
            using (var client = new SmtpClient("smtp.gmail.com", 587))
            {
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("phalandsabzi@gmail.com", "sajid286");

                #region
                string bodyHTML = @"
                    <div style='background-color:#fff;font-family:arial;font-size:14px;line-height:1.4;margin:0;padding:0'>
    <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate;background-color:#fff;width:100%'>
      <tbody><tr>
        <td style='font-family:arial;font-size:14px;vertical-align:top'>&nbsp;</td>
        <td style='font-family:arial;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:5px;width:580px;Margin:0 auto!important'>
          <div style='box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;border:1px #bbb1b1 solid'>
            <div style='float:left;width:100%;border-radius:0px;background:#f2f2f2;padding:0px 0px;text-align:center'><a style='display:inline-block' href='http://localhost:55514/Account/Login' target='_blank' data-saferedirecturl='https://www.google.com/url?hl=en&amp;q=http://localhost:55514/Account/Login&amp;source=gmail&amp;ust=1513324248794000&amp;usg=AFQjCNHMW7IjFeidgvRzSXE_LXkPcc6EQg'><img style='float:none;display:inline-block;width:140px' src='http://localhost:55514\Content\Logo office.png' alt='PhalSabzi logo' class='CToWUd'></a></div>
            <table class='' style='border-collapse:separate;background:#fff;border-radius:0px;width:100%'>
              <tbody><tr>
                <td style='font-family:arial;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px 45px'>
                  <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate;width:100%'>
                    <tbody><tr>
                         <td style='padding:15px 0px'>
                              <div style='float:left;width:100%;font-size:14px;color:#606060;font-family:arial'>Dear PhalSabzi Admin,</div>

                              <div style='float:left;width:100%;font-family:arial'>
                                   <div style='display:inline-block;margin-top:14px;font-size:14px;color:#606060;text-decoration:none'>
This is one of your client query mail form your Phalsabzi portal
<a href='http://localhost:55514/Account/Login' target='_blank' data-saferedirecturl='https://www.google.com/url?hl=en&amp;q=http://localhost:55514/Account/Login&amp;source=gmail&amp;ust=1513324248794000&amp;usg=AFQjCNF6McFN3p_vd_t8WgRYM4TUk4rO_g'>http://localhost:55514/Account/Login</a></div>

     <div style='display:inline-block;margin-top:14px;font-size:14px;color:#606060;text-decoration:none'>Pleae use following information to resolve users Quries

                                   <table style='color:#000;font-weight:bold;margin-top:15px'>
                                        <tbody><tr>
                                             <td style='padding:5px 10px 5px 0px'>User Name:</td>
                                             <td style='padding:5px 10px 5px 0px'>@name</td>
                                        </tr>
                                        <tr>
                                             <td style='padding:5px 10px 5px 0px'>User Email:</td>
                                             <td style='padding:5px 10px 5px 0px'><a href='mailto:@address' target='_blank'>@email</a></td>
                                        </tr
 <tr>
                                             <td style='padding:5px 5px 5px 0px'>User Phone:</td>
                                             <td style='padding:5px 10px 5px 0px'>@phone</td>
                                        </tr>
 <tr>
                                             <td style='padding:5px 10px 5px 0px'>Message:</td>
                                             <td style='padding:5px 10px 5px 0px'>@message</td>
                                        </tr>

                                   </tbody></table>
                              </div>
                         </td>
                    </tr>
                  </tbody></table>
                </td>
              </tr>
            </tbody></table>
            <div style='clear:both;width:100%'>
              <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate;width:100%'>
                <tbody><tr>
                  <td block m_914401766317106784content-block-footer' style='padding:0px 45px 35px'>
                    <span link' style='border-top:1px #fb9351 solid;color:#606060;font-size:14px;text-align:left;display:block;padding-top:35px'>
                         This email is auto generated and does not need a reply from you.To manage your account, you can login at 
                            <a href='http://localhost:55514/Account/Login' style='color:#fb9351!important;font-size:14px;text-decoration:none' target='_blank' data-saferedirecturl='https://www.google.com/url?hl=en&amp;q=http://studyabroad.pk&amp;source=gmail&amp;ust=1513324248794000&amp;usg=AFQjCNHNPVGwdNkrJbdTjx3DExFgG73e5w'>http://localhost:55514/Home/Contact</a>.
                    </span>
                  </td>
                </tr>
              </tbody></table>
            </div>
          </div>
        </td>
        <td style='font-family:arial;font-size:14px;vertical-align:top'>&nbsp;</td>
      </tr>
    </tbody></table><div class='yj6qo'></div><div class='adL'>
  </div></div> ";
                #endregion
                bodyHTML = bodyHTML.Replace("@name", mod.Name);
                bodyHTML = bodyHTML.Replace("@email", mod.Email);
                bodyHTML = bodyHTML.Replace("@phone", mod.Phone);
                bodyHTML = bodyHTML.Replace("@message", mod.Message);

                var message = new MailMessage();

                message.To.Add(new MailAddress("phalandsabzi@gmail.com"));
                message.Subject = mod.Subject;
                message.IsBodyHtml = true;
                message.Body = bodyHTML;
                message.IsBodyHtml = true;
                client.Send(message);
            }
            db.ContactUs.Add(mod);
            db.SaveChanges();
            TempData["Message"] = "Your message has been sent to owner!";
            return View();
        }

        public ActionResult NotFound()
        {
            return View();
        }

        public class cat
        {
            public string id { get; set; }
            public int count { get; set; }
        }
    }
}