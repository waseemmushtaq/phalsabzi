﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PhalSabzi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace PhalSabzi.Areas.Admin.Controllers
{
    public class RoleAssignController : Controller
    {
        PhalSabziEntities db = new PhalSabziEntities();
        // GET: Admin/RoleAssign
        public ActionResult Index()
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            ViewBag.contrler = asm.GetTypes().Where(type => typeof(Controller).IsAssignableFrom(type)).Select(x => x.Name);

            var list = db.AspNetRoles.ToList();
            return View(list);
        }

        public ActionResult CreateRole()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateRole(string roleName)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));

            if (roleName != null)
            {
                if (!roleManager.RoleExists(roleName))
                {
                    roleManager.Create(new IdentityRole(roleName));
                }
                TempData["Message"] = "Role Created!";

                //if (userManager.Users.Any(x => x.UserName == userName))
                //{
                //    var user = userManager.FindByName(userName);
                //    // Add User To Role
                //    if (userManager.IsInRole(user.Id, "Admin"))
                //    {
                //        userManager.RemoveFromRoles(user.Id, "Admin");
                //        TempData["Message"] = "Admin Rights Removed!";
                //    }
                //}
            }
            else
            {
                TempData["Message"] = "Role Already Exist!";

                //if (!roleManager.RoleExists("Admin"))
                //{
                //    roleManager.Create(new IdentityRole("Admin"));
                //}
                //if (userManager.Users.Any(x => x.UserName == userName))
                //{
                //    var user = userManager.FindByName(userName);
                //    // Add User To Role
                //    if (!userManager.IsInRole(user.Id, "Admin"))
                //    {
                //        userManager.AddToRole(user.Id, "Admin");
                //        TempData["Message"] = "Admin Rights Added!";
                //    }
                //}
            }
            return View();
        }

        [HttpPost]
        public JsonResult GetValues(myclass[] res)
        {
            return Json(new { res=true },JsonRequestBehavior.AllowGet);
        }

        public class myclass
        {
            public string rolename { get; set; }
            public string contname { get; set; }
        }
    }
}