﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhalSabzi.Models
{
    public class ProductViewModel
    {
        public  Item item { get; set; }
        public List<Item> items { get; set; }
        public List<Feedback> feedback { get; set; }
        public Cart cart { get; set; }

    }

    public class SearchViewModel
    {
        public List<Cart> Result { get; set; }
    }
}