﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PhalSabzi.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhalSabzi.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ItemsController : Controller
    {
        PhalSabziEntities db = new PhalSabziEntities();
        // GET: Admin/Items
        public ActionResult Index()
        {
            if (TempData["Message"] != null)
                ViewBag.Message = TempData["Message"].ToString();
            var data = db.Items.Where(x => x.Enable_Flag == "Y").ToList();
            return View(data);
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(Item mod, HttpPostedFileBase file)
        {
            try
            {
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = "";
                    var randomFile = "";
                    var uploadDir = "~/Items/Images";

                    System.IO.Directory.CreateDirectory(Server.MapPath(uploadDir));

                    fileName = file.FileName;
                    string extension = Path.GetExtension(file.FileName);
                    randomFile = DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + Guid.NewGuid().ToString("N") +
                                 extension;

                    var filePath = Path.Combine(Server.MapPath(uploadDir), randomFile);
                    file.SaveAs(filePath);

                    mod.Pic_Path = randomFile;
                    //mod.Pic_Title = fileName;
                }
                mod.Created_Date = DateTime.Now;
                mod.CreatedBy = User.Identity.GetUserId();
                mod.Enable_Flag = "Y";
                db.Items.Add(mod);
                db.SaveChanges();
                TempData["Message"] = "Item Add Successfully!";
                return RedirectToAction("Index");
            }
            catch (DbEntityValidationException dbx)
            {
                string message =
                    dbx.EntityValidationErrors.SelectMany(validationErrors => validationErrors.ValidationErrors)
                        .Aggregate("",
                            (current, validationError) =>
                                current +
                                $"Property: {validationError.PropertyName} Error: {validationError.ErrorMessage}");

                ModelState.AddModelError("", message);
                TempData["Message"] = message;
                return RedirectToAction("Index");
            }
        }

        public ActionResult Edit(int? id)
        {
            var find = db.Items.Where(x => x.ID == id && x.Enable_Flag == "Y").FirstOrDefault();
            return View(find);
        }

        [HttpPost]
        public ActionResult Edit(Item mod, HttpPostedFileBase file, string Img)
        {
            try
            {
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = "";
                    var randomFile = "";
                    var uploadDir = "~/Items/Images";

                    System.IO.Directory.CreateDirectory(Server.MapPath(uploadDir));

                    fileName = file.FileName;
                    string extension = Path.GetExtension(file.FileName);
                    randomFile = DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + Guid.NewGuid().ToString("N") +
                                 extension;

                    var filePath = Path.Combine(Server.MapPath(uploadDir), randomFile);
                    file.SaveAs(filePath);

                    mod.Pic_Path = randomFile;
                    //mod.Pic_Title = fileName;
                }
                else
                {
                    mod.Pic_Path = Img;
                }
                mod.Updated_Date = DateTime.Now;
                mod.UpdatedBy = User.Identity.GetUserId();
                mod.Name = mod.Name;
                mod.Enable_Flag = "Y";
                db.Entry(mod).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                TempData["Message"] = "Item Update Successfully!";
                return RedirectToAction("Index");
            }
            catch (DbEntityValidationException dbx)
            {
                string message =
                    dbx.EntityValidationErrors.SelectMany(validationErrors => validationErrors.ValidationErrors)
                        .Aggregate("",
                            (current, validationError) =>
                                current +
                                $"Property: {validationError.PropertyName} Error: {validationError.ErrorMessage}");

                ModelState.AddModelError("", message);
                TempData["Message"] = message;
                return RedirectToAction("Index");
            }
        }
       
        public ActionResult Delete(int? id)
        {
            var find = db.Items.Find(id);
            db.Items.Remove(find);
            //find.Enable_Flag = "D";
            db.SaveChanges();
            TempData["Message"] = "Item Delete Successfully!";
            return RedirectToAction("Index");
        }

        public ActionResult Dashboard()
        {
            return View();
        }

        public JsonResult DashboardCounters(int flag)
        {
            var Count = 0;

            switch (flag)
            {
                //count for Dashboard
                case 1:
                    Count = db.Items.Count();
                    break;
                case 2:
                    Count = db.AspNetUsers.Count();
                    break;
                case 3:
                    Count = db.Items.Count();
                    break;
                case 4:
                    Count = db.AspNetUsers.Count();
                    break;
                default:
                    Count = 0;
                    break;
            }
            return Json(new { count = Count });
        }

    }
}
