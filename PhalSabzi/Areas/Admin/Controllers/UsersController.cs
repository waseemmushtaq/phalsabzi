﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PhalSabzi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhalSabzi.Areas.Admin.Controllers
{
    public class UsersController : Controller
    {
        PhalSabziEntities db = new PhalSabziEntities();
        // GET: Admin/Users
        public ActionResult Index()
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var data = userManager.Users.ToList();
            return View(data);
        }

        public ActionResult Detail(string id)
        {
            var detail = db.AspNetUsers.Where(x => x.Id == id).FirstOrDefault();
            return View(detail);
        }

        public ActionResult AssignRole(string userName, string roleName)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));

            if (userName != null && roleName != null)
            {
                if (!roleManager.RoleExists("Admin"))
                {
                    roleManager.Create(new IdentityRole("Admin"));
                }

                if (userManager.Users.Any(x => x.UserName == userName))
                {
                    var user = userManager.FindByName(userName);
                    // Add User To Role
                    if (userManager.IsInRole(user.Id, "Admin"))
                    {
                        userManager.RemoveFromRoles(user.Id, "Admin");
                        TempData["Message"] = "Admin Rights Removed!";
                    }
                }
            }
            else
            {
                if (!roleManager.RoleExists("Admin"))
                {
                    roleManager.Create(new IdentityRole("Admin"));
                }
                if (userManager.Users.Any(x => x.UserName == userName))
                {
                    var user = userManager.FindByName(userName);
                    // Add User To Role
                    if (!userManager.IsInRole(user.Id, "Admin"))
                    {
                        userManager.AddToRole(user.Id, "Admin");
                        TempData["Message"] = "Admin Rights Added!";
                    }
                }
            }
            db.SaveChanges();
            return RedirectToAction("Dashboard");
        }


        public ActionResult UserShope(string id,string view)
        {
            //var userid = User.Identity.GetUserId();
            if (view=="shopping")
            {
                var usershop = db.Carts.Where(x => x.User_ID == id && x.Enable_Flag == "Confirmed").ToList();
                return PartialView("~/Areas/Admin/Views/Users/_UserShopePartial.cshtml", usershop);
            }
            else
            {
                var wishlist = db.Wishlists.Where(x => x.User_ID == id).ToList();
                return PartialView("~/Areas/Admin/Views/Users/_WishlistPartial.cshtml", wishlist);

            }
        }
    }
}