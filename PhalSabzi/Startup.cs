﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PhalSabzi.Startup))]
namespace PhalSabzi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
