//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PhalSabzi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Invoice
    {
        public int ID { get; set; }
        public string User_ID { get; set; }
        public string Total_Bill { get; set; }
        public string Delivery_Add { get; set; }
        public Nullable<System.DateTime> Creation_Date { get; set; }
        public string Enable_Flag { get; set; }
        public Nullable<int> Cart_ID { get; set; }
    
        public virtual Cart Cart { get; set; }
    }
}
