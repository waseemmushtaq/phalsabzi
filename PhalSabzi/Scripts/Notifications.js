﻿    $(document).ready(function () {
        var notifyMessage = $("#notifyMessage").val();
        if ($("#notifyMessage").val() != null) {
            if ($("#notifyMessage").val().length == 0) {
                console.log("blank - " + notifyMessage + " -blank");
            } else if ($("#notifyMessage").length > 0) {
                recordAdded(notifyMessage, "success");
            }
        }

    });

function recordAdded(message, type) {
    if (message === "Item Add Successfully!" || message === "Item Update Successfully!" || message === "Your Item is Added into Cart Successfully!" || message === "Your Feedback Added Successfully!" || message === "See Your Email for Confirmation Code!" || message === "Your Transection has been made Thank you have a nice day!" || message === "Your message has been sent to owner!") {
        showSuccessToast(message);
        //$.notify(" " + message, { type: type, icon: "check", align: "right", verticalAlign: "top" });
    }
    else if (message === "Item is Already in Wishlist!" ) {
        showWarningToast(message);
    }
    else {

        showDangerToast(message);
        //$.notify(" " + message, { type: type, icon: "close", align: "right", verticalAlign: "top", background: "#ffbebe", color: "#6a0000" });
    }
}
