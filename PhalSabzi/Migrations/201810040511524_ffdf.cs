namespace PhalSabzi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ffdf : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Enable_Flag", c => c.String());
            AddColumn("dbo.AspNetUsers", "Nude_Password", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Nude_Password");
            DropColumn("dbo.AspNetUsers", "Enable_Flag");
        }
    }
}
