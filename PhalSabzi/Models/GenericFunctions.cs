﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Net;
using System.Drawing;


namespace PhalSabzi.Models
{
    public class GenericFunctions
    {
        public static string ReturnDateFormat(DateTime dt, string Pattern)
        {
            string value = string.Empty;
            value = dt.ToString(Pattern);
            return value;
        }
        public static List<string> GetDateForChart()
        {
            DateTime dt = DateTime.Now;
            string datefrom = ReturnDateFormat(dt.AddDays(-31), "yyyy-MM-dd");
            string dateto = ReturnDateFormat(dt.AddDays(-1), "yyyy-MM-dd");
            List<string> lst = new List<string>();
            lst.Add(datefrom);
            lst.Add(dateto);
            return lst;
        }
        public static string GetPageRelativePath()
        {
            string url = HttpContext.Current.Request.Url.AbsolutePath;
            return url;
        }
        public static string getLinks(string name, string id)
        {
            string obj = "";
            if (name != null)
            {
                obj = RemoveWhiteSpaces(name) + "-" + id;
            }


            return obj;
        }
        public static string RemoveWhiteSpaces(string ab)
        {
            var re = new Regex("[;\\\\ / .,+=!@%#^)(${}~:*?\"<>|&']");
            ab = re.Replace(ab, "-");
            ab = ab.Replace("--", "-").Replace("--", "-");
            ab = ab.Replace(" ", "-").ToLower();
            ab = ab.TrimEnd('-');
            return ab;
        }
        public static string TrimData(string ab, int a)
        {
            string subab = ab;
            if (subab.Length == 0) return subab;
            if (subab.Length <= a) return subab;
            subab = subab.Substring(0, a);
            subab = subab + " ...";
            return subab;
        }
        public static string TrimString(string ab, int a)
        {
            string subab = ab;
            if (subab.Length == 0) return subab;
            if (subab.Length <= a) return subab;
            subab = subab.Substring(0, a);
            return subab;
        }
        public static string StripHTML(string HTMLText, bool decode = true)
        {
            Regex reg = new Regex("<[^>]+>", RegexOptions.IgnoreCase);

            var stripped = reg.Replace(HTMLText, "");
            var str1 = stripped.Replace("\r", "");
            var str2 = str1.Replace("\t", "");
            var strFinal = str2.Replace("\n", "");

            return decode ? HttpUtility.HtmlDecode(strFinal) : strFinal;
        }

        public static string GiveCaptcha()
        {
            Random Rand = new Random();
            int iNum = Rand.Next(100000, 999999);
            Bitmap Bmp = new Bitmap(120, 40);
            Graphics Gfx = Graphics.FromImage(Bmp);
            Font Fnt = new Font("Verdana", 16, FontStyle.Regular);
            Gfx.DrawString(iNum.ToString(), Fnt, Brushes.White, 15, 9);

            // Create random numbers for the first line 
            int RandY1 = Rand.Next(0, 40);
            int RandY2 = Rand.Next(0, 40);

            // Draw the first line 
            Gfx.DrawLine(Pens.Yellow, 0, RandY1, 120, RandY2);
            // Create random numbers for the second line 
            RandY1 = Rand.Next(0, 40);
            RandY2 = Rand.Next(0, 40);
            // Draw the second line 
            Gfx.DrawLine(Pens.Yellow, 0, RandY1, 120, RandY2);
            //Bmp.Save(HttpContext.Current.Response.OutputStream, ImageFormat.Gif);
            Bmp.Dispose();
            Gfx.Dispose();
            return iNum.ToString();
        }

    }
    public static class GenericExtentionFunctions
    {
        public static string GenrateURL(this string Input)
        {
            Input = (string)(Regex.Replace(Input, "[^\\w\\@-]", " ").ToLower());
            Input = Regex.Replace(Input, "\\s{1,}", "-");
            return Input;
        }
    }

    public class FieldOfStudty
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
