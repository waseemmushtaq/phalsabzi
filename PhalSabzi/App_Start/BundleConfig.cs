﻿using System.Web;
using System.Web.Optimization;

namespace PhalSabzi
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/indexbundle").Include(
                       "~/Content/adminassets/node_modules/datatables.net/js/jquery.dataTables.js",
                       "~/Content/adminassets/node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js",
                       "~/Content/adminassets/node_modules/jquery-toast-plugin/dist/jquery.toast.min.js",
                       "~/Content/adminassets/js/data-table.js",
                       "~/Content/adminassets/js/toastDemo.js",
                       "~/Scripts/notify.js",
                       "~/Scripts/Notifications.js"
                       ));
        }
    }
}
