﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PhalSabzi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PhalSabzi.Controllers
{
    public class ShoppingController : Controller
    {
        PhalSabziEntities db = new PhalSabziEntities();
        // GET: Shopping
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult FinalCart()
        {
            if (TempData["Message"] != null)
                ViewBag.Message = TempData["Message"].ToString();
            string id = User.Identity.GetUserId();
            var list = new List<Cart>();
            var cart = db.Carts.Where(x => x.User_ID == id && x.Enable_Flag == "New").ToList();
            foreach (var itemms in cart)
            {
                //if (itemms.Item.Sales != null)
                //{
                //    int sale = int.Parse(itemms.Item.Sales);
                //    double percent = (double)sale / 100;
                //    double ans = Convert.ToInt32(percent * itemms.Item.Price);
                //    itemms.Total = Convert.ToInt32(itemms.Item.Price - ans);
                //}
                list.Add(itemms);
            }
            return View(list);
        }


        [Authorize]
        public ActionResult Shope(clscode[] res)
        {
            if (res != null)
            {
                foreach (var item in res)
                {
                    var cart = db.Carts.Where(x => x.ID == item.itemid).FirstOrDefault();
                    if (cart != null)
                    {
                        cart.Total = item.price;
                        cart.Quantity = Convert.ToString(item.quantity);
                        cart.Item.Quantity = Convert.ToString(int.Parse(cart.Item.Quantity) - Convert.ToInt32(item.difference));
                        db.SaveChanges();
                    }
                }
                //db.SaveChanges();
                //TempData["Message"] = "Items Saved Successfully";
                //return RedirectToAction("FinalCart");
            }
            return Json(new { res = true }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Remove(int itemid, int quantity)
        {
            var remove = db.Carts.Find(itemid);
            remove.Item.Quantity = Convert.ToString(int.Parse(remove.Item.Quantity) + Convert.ToInt32(quantity));
            db.Carts.Remove(remove);
            db.SaveChanges();
            //TempData["Message"] = "Your Item is Deleted From Cart";
            return Json(new { res = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateCart()
        {
            return View();
        }

        public ActionResult Wishlist()
        {
                if (TempData["Message"] != null)
                {
                    ViewBag.Message = TempData["Message"].ToString();
                }
                var userid = User.Identity.GetUserId();
                var wishlist = db.Wishlists.Where(x => x.User_ID == userid).ToList();
                return View(wishlist);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Wishlist(int? id)
        {

            Wishlist mod = new Wishlist();
            var userid = User.Identity.GetUserId();
            var check = db.Wishlists.Where(x => x.User_ID == userid && x.Item_ID == id).FirstOrDefault();
            if (check == null)
            {
                mod.User_ID = userid;
                mod.Item_ID = id;
                mod.Creation_Date = DateTime.Now;
                mod.Enable_Flag = "Y";
                db.Wishlists.Add(mod);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            else
            {
                TempData["Message"] = "Item is Already in Wishlist!";
                var action = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();
                if (action == "Wishlist")
                {
                    return RedirectToAction("Wishlist", "Shopping");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
        }

        [Authorize]
        public ActionResult Invoice(Invoice mod)
        {
            var userId = User.Identity.GetUserId();
            mod.User_ID = userId;
            var cart = db.Carts.Where(x => x.User_ID == userId && x.Enable_Flag == "New").ToList();
            foreach (var item in cart)
            {
                item.Enable_Flag = "Confirmed";
                db.SaveChanges();
            }
            mod.Delivery_Add = db.AspNetUsers.Where(x => x.Id == mod.User_ID).Select(x => x.Address).FirstOrDefault();
            mod.Enable_Flag = "Confirm";
            mod.Creation_Date = DateTime.Now;
            db.Invoices.Add(mod);
            db.SaveChanges();
            TempData["Message"] = "Your Transection has been made Thank you have a nice day!";
            return Json(new { res = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InvoicePrint(rowcode[] res)
        {
            var list = new List<Cart>();
            Cart cart = new Cart();
            var userid = User.Identity.GetUserId();
            foreach (var item in res)
            {
                cart = db.Carts.Where(x => x.ID == item.itemid && x.User_ID == userid && x.Enable_Flag == "New").FirstOrDefault();
                if (cart != null)
                {
                    list.Add(cart);
                }
            }
            return PartialView("~/Views/Shopping/_InvoicePartial.cshtml", list);
        }

        [Authorize]
        public async Task<JsonResult> PermotionCode(PermotionCodeViewModel model)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId();
                if (userId == null)
                {
                    return Json("Error");
                }
                var user = await userManager.FindByIdAsync(userId);
                if (user == null)
                {
                    return Json("ForgotPasswordConfirmation");
                }
                // Send an email with this link
                string code = model.Code;
                await userManager.SendEmailAsync(user.Id, "Confirmation Code", "This is your Confirmation code while your Shopping at PhalSabzi.com.  \"" + code + "\" ");
                //TempData["Message"] = "See Your Email for Confirmation Code!";
                return Json(new { res = false }, JsonRequestBehavior.AllowGet);
            }

            // If we got this far, something failed, redisplay form
            return Json(new { res = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult cartdata()
        {
            string userid = User.Identity.GetUserId();
            var cart = db.Carts.Where(x => x.User_ID == userid && x.Enable_Flag == "New").ToList();
            if (cart != null && cart.Count != 0)
            {
                return PartialView("~/Views/Shopping/_Cartpartial.cshtml", cart);
            }
            else
            {
                return PartialView("~/Views/Shopping/_Cartpartial.cshtml", cart);
            }
        }

        [HttpPost]
        public JsonResult removecart(int? id, int? qaun)
        {
            string userid = User.Identity.GetUserId();
            var del = db.Carts.Where(x => x.User_ID == userid && x.Enable_Flag == "New" && x.Item_ID == id).FirstOrDefault();
            if (del != null)
            {
                del.Item.Quantity = Convert.ToString(int.Parse(del.Item.Quantity) + Convert.ToInt32(qaun));
                db.Carts.Remove(del);
                db.SaveChanges();
                return Json(new { res = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { res = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ItemsPartial()
        {
            string id = User.Identity.GetUserId();
            var list = new List<Cart>();
            var cart = db.Carts.Where(x => x.User_ID == id && x.Enable_Flag == "New").ToList();
            foreach (var itemms in cart)
            {
                list.Add(itemms);
            }
            return PartialView("~/Views/Shopping/_cartviewpartial.cshtml", list);
        }

        public ActionResult FAQ()
        {
            return View();
        }
        public class clscode
        {
            public int price { get; set; }
            public int itemid { get; set; }
            public int quantity { get; set; }
            public int sales { get; set; }
            public int difference { get; set; }

        }


        public class rowcode
        {
            public int itemid { get; set; }
        }
    }
}